<?php include_once('inc/header.php'); ?>
<body>
    <div class="banner_bg_main">
        <!-- header top section start -->
        <?php include_once('inc/nvabar.php'); ?>
        <!-- header top section start -->

        <!-- logo section start -->
        <?php include_once('inc/logo section.php'); ?>
        <!-- logo section end -->

        <!-- header section start -->
        <?php include_once('inc/header section.php'); ?>
        <!-- header section end -->

        <!-- banner section start -->
        <?php include('inc/banner section.php'); ?>
        <!-- banner section end -->
     </div> 

     <!-- slide number 1 -->
     <?php include_once('inc/slide number1.php'); ?>
     <!-- slide number 1 -->
     
    
     <!-- slide number 2 -->
     <?php include_once('inc/slide number2.php'); ?>
     <!-- slide number 2 -->

     <!-- slide number 3 -->
     <?php include_once('inc/slide number3.php'); ?>
     <!-- slide number 3 -->

     <!-- footer menu -->
     <?php include_once('inc/footer menu.php'); ?>
     <!-- footer menu -->

    <!-- the last footer -->
     <?php include_once('inc/the last footer.php'); ?>
    <!-- the last footer -->

     <!-- Javascript files-->
      <?php include_once('inc/javavscript files.php'); ?>
     <!-- Javascript files-->

     <!-- sidebar -->
     <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
     <script src="js/custom.js"></script>
     <script>
      function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
      }
      
      function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
      }
   </script>
     <!-- sidebar -->

</body>
</html>