<div class="banner_section layout_padding">
           <div class="container">
              <div id="my_slider" class="carousel slide" data-ride="carousel">
                 <div class="carousel-inner">
                
                    <div class="carousel-item active">
                       <div class="row">
                          <div class="col-sm-12">
                             <h1 class="banner_taital kh-font">តោះចាប់ផ្តើម <br>ទិញទំនិញដែលអ្នកចូលចិត្ត</h1>
                             <div class="buynow_bt kh-font"><a href="#">ទីញឥឡូវនេះ</a></div>
                          </div>
                       </div>
                    </div>

                    <div class="carousel-item">
                     <div class="row">
                        <div class="col-sm-12">
                           <h1 class="banner_taital kh-font">តោះចាប់ផ្តើម <br>ទិញទំនិញដែលអ្នកចូលចិត្ត</h1>
                           <div class="buynow_bt kh-font"><a href="#">ទីញឥឡូវនេះ</a></div>
                        </div>
                     </div>
                  </div>

                  <div class="carousel-item">
                     <div class="row">
                        <div class="col-sm-12">
                           <h1 class="banner_taital kh-font">តោះចាប់ផ្តើម <br>ទិញទំនិញដែលអ្នកចូលចិត្ត</h1>
                           <div class="buynow_bt kh-font"><a href="#">ទីញឥឡូវនេះ</a></div>
                        </div>
                     </div>
                  </div>

                 </div>
                 <a class="carousel-control-prev" href="#my_slider" role="button" data-slide="prev">
                 <i class="fa fa-angle-left"></i>
                 </a>
                 <a class="carousel-control-next" href="#my_slider" role="button" data-slide="next">
                 <i class="fa fa-angle-right"></i>
                 </a>
              </div>
           </div>
        </div>