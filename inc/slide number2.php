<div class="eletronic_section">
        
        <div id="electronic_main_slider" class="carousel slide" data-ride="carousel">
           <div class="carousel-inner">

              <div class="carousel-item active">
                 <div class="container">
                    <h1 class="fashion_taital kh-font">អេឡិចត្រូនិច</h1>
                    <div class="fashion_section_2">
                       <div class="row">
                          <div class="col-lg-4 col-sm-4">
                             <div class="box_main">
                                <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                                <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 200</span></p>
                                <p class="price_text kh-font">ឡើងទៅ</p>
                                <div class="electronic_img"><img src="images/laptop-img.png"></div>
                                <div class="btn_main">
                                    <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                    <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                                </div>
                             </div>
                          </div>

                          <div class="col-lg-4 col-sm-4">
                            <div class="box_main">
                               <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                               <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 100</span></p>
                               <p class="price_text kh-font">ឡើងទៅ</p>
                               <div class="electronic_img"><img src="images/mobile-img.png"></div>
                               <div class="btn_main">
                                <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                               </div>
                            </div>
                         </div>

                         <div class="col-lg-4 col-sm-4">
                            <div class="box_main">
                               <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                               <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 300</span></p>
                               <p class="price_text kh-font">ឡើងទៅ</p>
                               <div class="electronic_img"><img src="images/computer-img.png"></div>
                               <div class="btn_main">
                                  <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                  <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                               </div>
                            </div>
                         </div>

                          
                          
                       </div>
                    </div>
                 </div>
              </div>

              <div class="carousel-item">
               <div class="container">
                  <h1 class="fashion_taital kh-font">អេឡិចត្រូនិច</h1>
                  <div class="fashion_section_2">
                     <div class="row">
                        <div class="col-lg-4 col-sm-4">
                           <div class="box_main">
                              <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                              <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 200</span></p>
                              <p class="price_text kh-font">ឡើងទៅ</p>
                              <div class="electronic_img"><img src="images/laptop-img.png"></div>
                              <div class="btn_main">
                                  <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                  <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-4 col-sm-4">
                          <div class="box_main">
                             <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                             <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 100</span></p>
                             <p class="price_text kh-font">ឡើងទៅ</p>
                             <div class="electronic_img"><img src="images/mobile-img.png"></div>
                             <div class="btn_main">
                              <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                              <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                             </div>
                          </div>
                       </div>

                       <div class="col-lg-4 col-sm-4">
                          <div class="box_main">
                             <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                             <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 300</span></p>
                             <p class="price_text kh-font">ឡើងទៅ</p>
                             <div class="electronic_img"><img src="images/computer-img.png"></div>
                             <div class="btn_main">
                                <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                             </div>
                          </div>
                       </div>

                        
                        
                     </div>
                  </div>
               </div>
            </div>

            <div class="carousel-item">
               <div class="container">
                  <h1 class="fashion_taital kh-font">អេឡិចត្រូនិច</h1>
                  <div class="fashion_section_2">
                     <div class="row">
                        <div class="col-lg-4 col-sm-4">
                           <div class="box_main">
                              <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                              <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 200</span></p>
                              <p class="price_text kh-font">ឡើងទៅ</p>
                              <div class="electronic_img"><img src="images/laptop-img.png"></div>
                              <div class="btn_main">
                                  <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                  <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-4 col-sm-4">
                          <div class="box_main">
                             <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                             <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 100</span></p>
                             <p class="price_text kh-font">ឡើងទៅ</p>
                             <div class="electronic_img"><img src="images/mobile-img.png"></div>
                             <div class="btn_main">
                              <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                              <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                             </div>
                          </div>
                       </div>

                       <div class="col-lg-4 col-sm-4">
                          <div class="box_main">
                             <h4 class="shirt_text kh-font">កុំព្យូទ័រ</h4>
                             <p class="price_text kh-font">តម្លៃចាប់ពី  <span style="color: #262626;">$ 300</span></p>
                             <p class="price_text kh-font">ឡើងទៅ</p>
                             <div class="electronic_img"><img src="images/computer-img.png"></div>
                             <div class="btn_main">
                                <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                             </div>
                          </div>
                       </div>

                        
                        
                     </div>
                  </div>
               </div>
            </div>

           </div>
           <a class="carousel-control-prev" href="#electronic_main_slider" role="button" data-slide="prev">
           <i class="fa fa-angle-left"></i>
           </a>
           <a class="carousel-control-next" href="#electronic_main_slider" role="button" data-slide="next">
           <i class="fa fa-angle-right"></i>
           </a>
        </div>
     </div>