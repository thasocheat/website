<div class="header_section">
           <div class="container">
              <div class="containt_main">
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <a class="kh-font" href="#">អំពីយើង</a>
                    <a class="kh-font" href="#">សេវាកម្ម</a>
                    <a class="kh-font" href="#">អតិថិជន</a>
                    <a class="kh-font" href="#">ទំនាកទំនង</a>
                  </div>
                 <span class="toggle_icon" onclick="openNav()"><img src="images/toggle-icon.png"></span>

                 <div class="dropdown">
                    <button class="dropbtn btn-secondary​ kh-font">ប៉ះដើម្បីបង្ហាញ<img style="width: 20px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAAC2UlEQVRoge2ZQU/UQBiGn7ZbjcSLoAcFz3pHiR7xNxg0+guIeyRsIoQsQZcGOAAxFJAzISvuQY1HvRu9GC96Q4mJgjcT4bLjoZSspd1+bWfLGvZN3tPON33e6Ux3OoWOOjrZMjT1YwEDwCDQD1wFLgJnD37/DXwHPgPvgbfAO6Cu6fqpdRlwgG1AJfQ3YBroy50auACsAPtC2GbeB5aAnrzg7wG/NIAHvQvcaSW4DTxtAXjQLlDQDd8FvM4B3verg2tqkZ0zfGMILXcij2kT5aWs8PePEd73UFr4HmCnDQLsAufTBFhpA3jfT5LC96HnT0qX94DeMFAzIkAROJU0dQt1GnggbWzi7VOOe9SD3sbbNMbqZhvARnkgbLSDGpSkBCiXy0xMTEibh9Y7jpOk5JakUQ3BaJTLZeVrfHw88WhOTk4e1juOI617JgnwKQm8r7GxMTH81NTUkXphiI+SAE23ypZlqVqtdgRAKaVGRkZiIWZnZ0NrNzc3lWVZcfU7kgCxz3/btiNDlEqlyLpKpRJaU61WlW3bkjuwpyVAXIjR0dF/2hqGoebn50PbbmxsqEKhIJ1+ogDit62oEPV6XRWLxUP4hYWFUPj19fUk8OIpFLuIpSGGh4eV67qh8Gtra8o0zSTw4kX8PGGnTadTmFZXV9PAK6AqCfAwRcfiEMvLy2nhFVCSBLiRsvPYEK7rKsMw0sIr4LokgAl81R1ibm4uC7gCtojePR/RdJaLBUPMzMxkhVfAYyk8aHih8UMk2Oc0c+QLTTMtZb1whsUa9GJSeIBu4KcmgCzeJcO56d02CHA7Lbwv9xjhU02doCyELzma/RKNh7xdeGeVecG/AM7ogvdVQMOTSeBFWnC83qghWnPk+AMNC1aqbrzjvj0N4H/wRv1cXvCN6gUqpNs7bQGPgEtZAHR9ZjWBa3jnNv3AFbxwjZ9Zt4EveJ9Z3wAfaIPPrB119L/rLypQ9PqT9LW5AAAAAElFTkSuQmCC"></button>
                    <div class="dropdown-content">
                      <a class="kh-font" href="#">ផ្លែឈី</a>
                      <a class="kh-font" href="#">អេឡិចត្រូនិច</a>
                      <a class="kh-font" href="#">សំភារៈផ្ទះបាយ</a>
                    </div>
                  </div>
                 

                 <div class="main">
                    <!-- Another variation with a button -->
                    <div class="input-group">
                       <input type="text" class="form-control​ kh-font " style="font-size:medium; width: 90%;" placeholder="ស្វែងរកនៅខាងក្នុងនេះ">
                       <div class="input-group-append">
                          <button class="btn btn-secondary" type="button" style="background-color: #f26522; border-color:#f26522 ">
                          <i class="fa fa-search"></i>
                          </button>
                       </div>
                    </div>
                 </div>
                
                  <div class="dropdown">
                    <button class=" btn-secondary​ kh-font  bg-white"><img style="width: 15px;" src="images/cambodia.png" alt="flag" class="mr-2 " title="Kingdom Of Cambodia"> Khmer <i class="fa fa-angle-down ml-2" aria-hidden="true"></i></button>
                    <div class="dropdown-content">
                        <a href="#" class="dropdown-item">
                            <img src="images/flag-uk.png" class="mr-2" alt="flag">
                            English
                        </a>
                        <a href="#" class="dropdown-item">
                            <img src="images/flag-france.png" class="mr-2" alt="flag">
                            French
                        </a>
                    </div>
                  </div>
                 <div class="header_box ">
                    <div class="login_menu">
                       <ul>
                          <li><a href="#">
                             <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                             <span class="padding_10 kh-font">កាត</span></a>
                          </li>
                          <li><a href="#">
                             <i class="fa fa-user" aria-hidden="true"></i>
                             <span class="padding_10 kh-font">កាត</span></a>
                          </li>
                       </ul>
                    </div>
                 </div>
              </div>
           </div>
        </div>