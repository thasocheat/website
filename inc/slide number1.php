<div class="fruit_section">
        <div id="main_slider" class="carousel slide" data-ride="carousel">
           <div class="carousel-inner">
              
              <div class="carousel-item active">
                 <div class="container">
                    <h1 class="fashion_taital kh-font">បន្លែ &amp; ផ្លែឈី</h1>
                    <div class="fashion_section_2">
                       <div class="row">

                          <div class="col-lg-4 col-sm-4">
                             <div class="box_main">
                                <h4 class="shirt_text kh-font">ផ្លែក្រូច</h4>
                                <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ១០០០</span></p>
                                <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                                <div class="tshirt_img"><img src="images/orange.jpg"></div>
                                
                                <div class="btn_main">
                                   <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                   <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                                </div>
                             </div>
                          </div>

                          <div class="col-lg-4 col-sm-4">
                            <div class="box_main">
                               <h4 class="shirt_text kh-font">ស្ពៃក្តាប</h4>
                               <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ២០០០</span></p>
                               <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                               <div class="tshirt_img"><img src="images/cabbage.jpg"></div>
                               
                               <div class="btn_main">
                                  <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                  <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                               </div>
                            </div>
                         </div>

                         <div class="col-lg-4 col-sm-4">
                            <div class="box_main">
                               <h4 class="shirt_text kh-font">ត្រសក់</h4>
                               <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ១៥០០</span></p>
                               <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                               <div class="tshirt_img"><img src="images/cucumber.jpg"></div>
                               
                               <div class="btn_main">
                                  <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                  <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                               </div>
                            </div>
                         </div>
                          
                       </div>
                    </div>
                 </div>
              </div>

              <div class="carousel-item">
               <div class="container">
                  <h1 class="fashion_taital kh-font">បន្លែ &amp; ផ្លែឈី</h1>
                  <div class="fashion_section_2">
                     <div class="row">

                        <div class="col-lg-4 col-sm-4">
                           <div class="box_main">
                              <h4 class="shirt_text kh-font">ផ្លែក្រូច</h4>
                              <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ១០០០</span></p>
                              <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                              <div class="tshirt_img"><img src="images/orange.jpg"></div>
                              
                              <div class="btn_main">
                                 <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                 <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-4 col-sm-4">
                          <div class="box_main">
                             <h4 class="shirt_text kh-font">ស្ពៃក្តាប</h4>
                             <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ២០០០</span></p>
                             <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                             <div class="tshirt_img"><img src="images/cabbage.jpg"></div>
                             
                             <div class="btn_main">
                                <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                             </div>
                          </div>
                       </div>

                       <div class="col-lg-4 col-sm-4">
                          <div class="box_main">
                             <h4 class="shirt_text kh-font">ត្រសក់</h4>
                             <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ១៥០០</span></p>
                             <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                             <div class="tshirt_img"><img src="images/cucumber.jpg"></div>
                             
                             <div class="btn_main">
                                <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                             </div>
                          </div>
                       </div>
                        
                     </div>
                  </div>
               </div>
            </div>

            <div class="carousel-item">
               <div class="container">
                  <h1 class="fashion_taital kh-font">បន្លែ &amp; ផ្លែឈី</h1>
                  <div class="fashion_section_2">
                     <div class="row">

                        <div class="col-lg-4 col-sm-4">
                           <div class="box_main">
                              <h4 class="shirt_text kh-font">ផ្លែក្រូច</h4>
                              <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ១០០០</span></p>
                              <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                              <div class="tshirt_img"><img src="images/orange.jpg"></div>
                              
                              <div class="btn_main">
                                 <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                 <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-4 col-sm-4">
                          <div class="box_main">
                             <h4 class="shirt_text kh-font">ស្ពៃក្តាប</h4>
                             <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ២០០០</span></p>
                             <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                             <div class="tshirt_img"><img src="images/cabbage.jpg"></div>
                             
                             <div class="btn_main">
                                <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                             </div>
                          </div>
                       </div>

                       <div class="col-lg-4 col-sm-4">
                          <div class="box_main">
                             <h4 class="shirt_text kh-font">ត្រសក់</h4>
                             <p class="price_text kh-font">តម្លៃ  <span style="color: #262626;">៛ ១៥០០</span></p>
                             <p class="price_text kh-font">ក្នុង  <span style="color: #262626;">១គីឡូ</span></p>
                             <div class="tshirt_img"><img src="images/cucumber.jpg"></div>
                             
                             <div class="btn_main">
                                <div class="buy_bt kh-font"><a href="#">ទិញឥឡូវនេះ</a></div>
                                <div class="seemore_bt kh-font"><a href="#">មើលច្រើនទៀត</a></div>
                             </div>
                          </div>
                       </div>
                        
                     </div>
                  </div>
               </div>
            </div>

              
           </div>
           <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
           <i class="fa fa-angle-left"></i>
           </a>
           <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
           <i class="fa fa-angle-right"></i>
           </a>
        </div>
     </div>